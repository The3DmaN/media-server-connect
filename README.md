# Media Server Connect

Media Sever Connect is a simple web app for connecting to the web interface of an Emby server. 

It is designed for Linux mobile but may also run on other platforms.


# Install:

Manjaro/Arch:

- Install directly from AUR (https://aur.archlinux.org/packages/media-server-connect-git/)

# Known Issues

**Manjaro Plasma Mobile** - Menu button in header does not work

Workaround: Swipe to right to open

Bug submission: https://gitlab.manjaro.org/manjaro-arm/issues/pinephone/plasma-mobile/-/issues/36

# Screenshots:

![Screenshot](img/media-server-connect_screenshot.png "MSC Screenshot")

# Important Note:

This app is not an official Emby app and is not maintained or supported by the Emby team.


