import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11
import Qt.labs.settings 1.1
//import QtWebEngine 1.10



Page {
    id: page
    title: qsTr("Settings")

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 20

        ColumnLayout {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            RowLayout {
                visible: true
                spacing: 10
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

                Label {
                    id: iplabel
                    text: qsTr("IP Address:")
                    horizontalAlignment: Text.AlignLeft
                    Layout.preferredWidth: -1
                }

                TextField {
                    id: ipinput
                    placeholderText: qsTr("192.168.X.X")
                    text: ipsettings.ip
                }
            }

            RowLayout {
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                spacing: 10

                Label {
                    id: portlabel
                    text: qsTr("Port:")
                    horizontalAlignment: Text.AlignLeft
                    Layout.preferredWidth: -1
                }

                TextField {
                    id: portinput
                    Layout.maximumWidth: 80
                    placeholderText: qsTr("1234")
                    text: ipsettings.port
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignBottom | Qt.AlignHCenter

            Button {
                id: savesettings
                text: qsTr("Save Settings")

                Connections {
                    target: savesettings
                    onClicked: {
                        ipsettings.port = portinput.text
                        ipsettings.ip = ipinput.text
                        stackView.pop()
                        stackView.replace("MediaServerConnect.ui.qml")
                    }
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
