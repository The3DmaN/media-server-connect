import QtQuick 2.12
import QtQuick.Controls 2.5
import QtWebEngine 1.10
import Qt.labs.settings 1.1

Page {
    title: qsTr("Media Server Connect")

    WebEngineView {
        id: webview
        anchors.fill: parent
        settings.allowWindowActivationFromJavaScript: false
        settings.javascriptCanOpenWindows: true
        settings.javascriptEnabled: true
        settings.webGLEnabled: true
        settings.fullScreenSupportEnabled: true
        settings.showScrollBars: false
        url: "http://" + ipsettings.ip + ":" + ipsettings.port

        Connections {
            target: webview
            onFullScreenRequested: {
                if (request.toggleOn) {

                    window.showFullScreen()
                    toolBar.visible = false
                    request.accept()

                } else {

                    window.showNormal()
                    toolBar.visible = true
                    request.accept()
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
